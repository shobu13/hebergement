import csv

import googlemaps
from django.core.management import BaseCommand
from core.models import User, Host


class Command(BaseCommand):

    def handle(self, *args, **options):
        with open('hosters.csv', newline='\n') as file:
            csv.register_dialect('dialect', delimiter=';')
            reader = csv.DictReader(file, dialect='dialect')
            data = [i for i in reader]
        for i in data:
            gmap = googlemaps.Client(key='AIzaSyA3rLSjB3YgoIwXw6VOKN9FJAYh1Kd444E')
            results = gmap.find_place(i['ville'], 'textquery')['candidates']
            if results:
                place = gmap.place(results[0]['place_id'])['result']
                print(place)
                user = User()
                user.first_name = i['prenom']
                user.last_name = i['nom']
                user.email = i['email']
                user.username = i['email']
                place_name = place['address_components'][1]['long_name']
                location = place['geometry']['location']
                user.city_name = place_name
                user.city_lat = location['lat']
                user.city_lng = location['lng']
                user.phone_number = i['telephone']
                user.description = i['champs_1']
                user.host = Host()

                user.save()
